nodes := $(patsubst nodes/%.yml,%,$(wildcard nodes/*.yml))
profiles-di = $(patsubst %,content/%/preseed.cfg,$(subst -,/,$(nodes)))
profiles-script = $(patsubst %/preseed.cfg,%/script.sh,$(profiles-di))
profiles = $(profiles-di) $(profiles-script)
mdfiles = content/MD5SUMS
shafiles = $(patsubst %,content/SHA%SUMS,1 256 512)
checksumfiles = $(mdfiles) $(shafiles)
signfiles = $(patsubst %,%.sig,$(checksumfiles))
info = content/NEWS content/TODO

suite ?= jessie

all: $(checksumfiles) $(info)
sign: $(signfiles)

$(profiles-di): content/%/preseed.cfg:
	mkdir -p $(dir $@)
	cd $(dir $@) \
		&& boxer compose \
			--nodedir $(CURDIR)/nodes \
			--skeldir $(CURDIR)/skel \
			--suite $(suite) \
			$(subst /,-,$*)

$(profiles-script): content/%/script.sh: content/%/preseed.cfg

$(mdfiles): content/MD%SUMS: $(profiles)
	cd content && md$*sum $(profiles:content/%=%) > MD$*SUMS~
	rm -f $@.sig
	mv -f $@~ $@

$(shafiles): content/SHA%SUMS: $(profiles)
	cd content && sha$*sum $(profiles:content/%=%) > SHA$*SUMS~
	rm -f $@.sig
	mv -f $@~ $@

$(signfiles): %.sig: % $(checksumfiles)
	rm -f $@
	gpg --detach-sign -a -o $@ $<

$(info): content/% : %
	mkdir -p content
	cp -f $< $@

clean:
	rm -rf content
