Source: debian-parl
Section: misc
Priority: optional
Maintainer: DebianParl team <parl-devel@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Build-Depends: cdbs,
 debhelper,
 boxer (>= 0.004~),
 shellcheck,
# lock to boxer-data minor release: Feature additions should be documented
 boxer-data (>= 10.8),
 boxer-data (<< 10.10)
Standards-Version: 4.6.0
Homepage: https://wiki.debian.org/DebianParl
Vcs-Git: https://salsa.debian.org/blends-parl-team/blends.git
Vcs-Browser: https://salsa.debian.org/blends-parl-team/blends
Rules-Requires-Root: no

Package: parl-data
Architecture: all
Depends: ${misc:Depends}
Description: recipes to install DebianParl blends
 This package provides "recipes" for installing the DebianParl blends,
 in the form of preseeding files for Debian Installer.
 .
 DebianParl is a subset of Debian for use by parliamentarians,
 politicians and their staffers all around the world.

Package: parl-desktop
Architecture: all
Depends: ${cdbs:Depends},
 ${misc:Depends}
Suggests: parl-desktop-strict
Description: DebianParl desktop for parliamentary work
 This metapackage provides the core DebianParl/desktop profile.
 .
 DebianParl/desktop establish a desktop/laptop environment for
 parliamentary work.
 .
 DebianParl is a subset of Debian for use by parliamentarians,
 politicians and their staffers all around the world.

Package: parl-desktop-world
Architecture: all
Depends: parl-desktop,
 ${cdbs:Depends},
 ${misc:Depends}
Suggests: parl-desktop-strict
Description: DebianParl desktop for parliamentary work - global
 This metapackage provides a DebianParl/desktop profile
 for global use.
 .
 DebianParl/desktop establish a desktop/laptop environment for
 parliamentary work.
 .
 DebianParl is a subset of Debian for use by parliamentarians,
 politicians and their staffers all around the world.

Package: parl-desktop-eu
Architecture: all
Depends: parl-desktop,
 ${cdbs:Depends},
 ${misc:Depends}
Suggests: parl-desktop-strict
Description: DebianParl desktop for parliamentary work - EU
 This metapackage provides a DebianParl/desktop profile
 for use within the European Union.
 .
 DebianParl/desktop establish a desktop/laptop environment for
 parliamentary work.
 .
 DebianParl is a subset of Debian for use by parliamentarians,
 politicians and their staffers all around the world.

Package: parl-desktop-strict
Architecture: all
Depends: parl-desktop,
 ${misc:Depends}
Conflicts: ${cdbs:Conflicts}
Description: DebianParl desktop for parliamentary work - strict
 This metapackage apply restrictions for the DebianParl/desktop profiles
 to avoid optional stuff.
 .
 DebianParl/desktop establish a desktop/laptop environment for
 parliamentary work.
 .
 DebianParl is a subset of Debian for use by parliamentarians,
 politicians and their staffers all around the world.
